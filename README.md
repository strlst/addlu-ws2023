<div align="center">

# Advanced Digital Design [LU] - WS 2023
Florian Huemer, Andreas Steininger, Dylan Baumann
</div>

This is the assignment repository for the Advanced Digital Design Lab Course.
The course consists of the following exercises (we will push updates to this repository during the semester):

 * [meta](meta/assignment.md)
 * [cdc](cdc/assignment.md)
 * [stgs](stgs/assignment.md)

The exercises don't build upon each other, so you can work on them in any order.

Document your work using a presentation, which also doubles as your lab report!
Make sure that all steps you took during circuit design, measurement and analysis are reflected by this report.
At the end of the semester you will use these slides to present you work to the teaching staff and the other groups.
You are also asked to upload your design files to TUWEL.

After each experiment in the lab, please switch off all devices that you used!

