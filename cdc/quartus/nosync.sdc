create_clock -name "clk50" -period 20.000ns [get_ports {clk50}]

create_clock -name "clk_src" -period 16.667ns [get_ports {clk_src}]
create_clock -name "clk_dst" -period 16.667ns [get_ports {clk_dst}]

derive_pll_clocks -create_base_clocks

derive_clock_uncertainty
