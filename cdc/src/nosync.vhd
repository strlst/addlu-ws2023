library ieee;
use ieee.std_logic_1164.all;

entity top is
  port (
  clk50  : in std_logic;
  
  	-- onboard SMA connectors
	sma_clkin : in std_logic;  -- AH14
	sma_clkout : out std_logic; -- AE23
  
    clk_src     : in  std_logic;
    clk_dst     : in  std_logic;
    reset_n     : in  std_logic;
    data_src    : out std_logic_vector(11 downto 0);
    req         : out std_logic;
    data_dst    : out std_logic_vector(11 downto 0);
    ack         : out std_logic;
    clk_src_out : out std_logic_vector(1 downto 0);
    clk_dst_out : out std_logic_vector(1 downto 0)
    );
end entity;

library ieee;
use ieee.std_logic_1164.all;

entity sync is
  generic (
    STAGES : natural := 2
  );
  port (
    clk      : in  std_logic;
    reset_n  : in  std_logic;
    data_in  : in  std_logic;
    data_out : out std_logic
  );
end entity;

library ieee;
use ieee.std_logic_1164.all;

entity src is
  port (
    clk     : in  std_logic;
    reset_n : in  std_logic;
    ack     : in std_logic;
    data    : out std_logic_vector(11 downto 0);
    req     : out std_logic
    );
end entity;

library ieee;
use ieee.std_logic_1164.all;

entity dst is
  port (
    clk      : in  std_logic;
    reset_n  : in  std_logic;
    data     : in  std_logic_vector(11 downto 0);
    req      : in  std_logic;
    ack      : out std_logic;
    data_out : out std_logic_vector(11 downto 0)
    );
end entity;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture src_arch of src is
  signal data_tgl            : std_logic_vector(7 downto 0);
  attribute keep             : boolean;
  attribute keep of data_tgl : signal is true;
  signal data_cnt            : unsigned(3 downto 0);
  signal regreq, regack      : std_logic;
  signal regackold           : std_logic;
  signal ack_edge            : std_logic;
  signal reset_sync          : std_logic_vector(1 downto 0);
begin
  rst : process(clk, reset_n)
  begin
    if reset_n = '0' then
      reset_sync <= (others => '0');
    elsif rising_edge(clk) then
      reset_sync <= '1' & reset_sync(1);
    end if;
  end process rst;

  -- use simple 2-phase protocol
  --ack_edge <= '1' when regreq = ack else '0';
  -- detect transitions
  ack_edge <= '1' when regackold /= regack else '0';

  sync : process(clk, reset_sync)
  begin
    if reset_sync(0) = '0' then
      data_tgl  <= "10101010";
      data_cnt  <= to_unsigned(0, data_cnt'length);
      regreq    <= '0';
      regack    <= '0';
      regackold <= '0';
    elsif rising_edge(clk) then
      regreq <= regreq;
      regack <= ack;
      regackold <= regack;
      if ack_edge = '1' then
	      data_tgl <= not data_tgl;
	      data_cnt <= data_cnt + 1;
	      regreq <= not regreq;
      end if;
    end if;
  end process sync;

  data <= data_tgl & std_logic_vector(data_cnt);
  req  <= regreq;
end architecture;

architecture dst_arch of dst is
  signal reset_sync     : std_logic_vector(1 downto 0);
  signal datareg        : std_logic_vector(data'range);
  signal regack, regreq : std_logic;
  signal regreqold      : std_logic;
  signal req_edge       : std_logic;
begin
  rst : process(clk, reset_n)
  begin
    if reset_n = '0' then
      reset_sync <= (others => '0');
    elsif rising_edge(clk) then
      reset_sync <= '1' & reset_sync(1);
    end if;
  end process rst;

  -- use simple 2-phase protocol
  --req_edge <= '1' when regack /= req else '0';
  -- detect transitions
  req_edge <= '1' when regreqold /= regreq else '0';

  reg : process(clk, reset_sync, req)
  begin
    if reset_sync(0) = '0' then
      datareg <= (others => '0');
      regack <= '1';
      regreq <= '0';
      regreqold <= '0';
    elsif rising_edge(clk) then
      regack <= regack;
      regreq <= req;
      regreqold <= regreq;
      if req_edge = '1' then
	regack <= not regack;
        datareg <= data;
      end if;
    end if;
  end process reg;

  data_out <= datareg;
  ack <= regack;
end architecture;

architecture sync_arch of sync is
	signal reg : std_logic_vector(STAGES downto 0);
begin
	data_out <= reg(STAGES);
	sync : process(clk, reset_n)
	begin
		if reset_n = '0' then
			reg <= (others => '0');
		elsif rising_edge(clk) then
			reg(STAGES downto 1) <= reg(STAGES - 1 downto 0);
			reg(0) <= data_in;
		end if;
	end process;
end architecture;

architecture top_arch of top is
  signal src_data : std_logic_vector(11 downto 0);
  signal src_req, snk_req  : std_logic;
  signal snk_ack, src_ack  : std_logic;
begin

  mypll_inst : entity work.mypll
  port map(
    inclk0 => clk50,
    c0     => sma_clkout
  );

  src_inst : entity work.src
    port map (
      clk     => clk_src,
      reset_n => reset_n,
      --ack     => snk_ack,
      -- use synchronized signal
      ack     => src_ack,
      data    => src_data,
      req     => src_req
  );

  dst_inst : entity work.dst
    port map (
      clk      => clk_dst,
      reset_n  => reset_n,
      data     => src_data,
      --req      => src_req,
      -- use synchronized signal
      req      => snk_req,
      ack      => snk_ack,
      data_out => data_dst
  );

  sync_req : entity work.sync
  generic map (
    STAGES => 2
  )
  port map (
    clk      => clk_src,
    reset_n  => reset_n,
    data_in  => src_req,
    data_out => snk_req
  );

  sync_ack : entity work.sync
  generic map (
    STAGES => 2
  )
  port map(
    clk      => clk_src,
    reset_n  => reset_n,
    data_in  => snk_ack,
    data_out => src_ack
  );

  clk_src_out <= (others => clk_src);
  clk_dst_out <= (others => clk_dst);
  data_src    <= src_data;
  --req         <= src_req;
  --ack         <= snk_ack;
  -- use synchronized data
  req         <= snk_req;
  ack         <= src_ack;

end architecture;
