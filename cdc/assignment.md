
# Clock Domain Crossing

Open the provided quartus project (`make quartus_gui` in folder quartus), synthesize the design and download it to the DE2-115 FPGA board with the SMA daughter board.
Do not forget to adjust the timing assumptions (.sdc file).
The design contains a data source and a data sink that can be clocked independently via the top-level inputs `clk_src` and `clk_dst`.
The source produces a new data word (12 bit) every clock cycle (`data_src`); the 8 most significant bits alternate between the values "0x55" and "0xAA" and the 4 least significant bits implement a counter that increments by one with each step.
The receiver’s task is to capture the data word using its own clock and make it available at port `data_dst`.
A two-phase encoded request line is used to indicate valid new data (signal `req`).

Connect a clock source with a fixed frequency of 60 MHz to the receiver’s clock input `clk_dst`, i.e., use an SMA cable and connect the output `J15`/`sma_clkout` of the FPGA board with the input `J28`/`clk_dst` of the daughter board.

For the sender’s clock (`clk_src`) use a 60 MHz clock source as well, but modulate it by +/-1 MHz with a modulation frequency of 100 Hz using a triangular modulation waveform.
Use the Agilent/Keysight 33250A Arbitrary Waveform Generator for this purpose.
If you are not aware how they have to be set up for the desired waveform take a look at the user manual available on the web.
To control the function generators over USB we provide you with a simple Python script in the `tools` folder.
The file [README](tools/README.md) contains a brief description of the command line interface.
Connect the waveform generator to the `J26`/`clk_src` input of the daughter board.
Be careful with the clock generator impedance settings: Don't use 50 Ohms output impedance!
If you are not sure what you are doing, ask a supervisor or tutor before starting the signal generator.

Use the Agilent/Keysight 16803A logic analyzer to observe the behavior of the circuit.
You will notice two error sources: missing flow control (skipped and doubly received data words) and missing synchronization (wrong/inconsistent data).
Show an example for each in your presentation and measure the error rate.

The drawing below shows the pinout of the GPIO connector of the FPGA board.
The identifiers in parenthesis beside the actual pin denotes the name of the I/O pin of the FPGA as used in the Pin Planner in Quartus.
The SMA connector `J25` (`J27`) on the daughter board also outputs `clk_src_out` (`clk_dst_out`).

```
                    +--+
 clk_src_out (AB22) |oo| (AC15) clk_dst_out
         req (AB21) |oo| (Y17) ack
             (AC21) |oo| (Y16)
             (AD21) |oo| (AE16)
             (AD15) |oo| (AE15)
                5 V |oo| GND
 data_src(0) (AC19) |oo| (AF16) data_dst(0)
 data_src(1) (AD19) |oo| (AF15) data_dst(1)
 data_src(2) (AF24) |oo| (AE21) data_dst(2)
 data_src(3) (AF25)  oo| (AC22) data_dst(3)
 data_src(4) (AE22)  oo| (AF21) data_dst(4)
 data_src(5) (AF22) |oo| (AD22) data_dst(5)
 data_src(6) (AG25) |oo| (AD25) data_dst(6)
 data_src(7) (AH25) |oo| (AE25) data_dst(7)
              3.3 V |oo| GND
 data_src(8) (AG22) |oo| (AE24) data_dst(8)
 data_src(9) (AH22) |oo| (AF26) data_dst(9)
data_src(10) (AE20) |oo| (AG23) data_dst(10)
data_src(11) (AF20) |oo| (AH26) data_dst(11)
             (AH23) |oo| (AG26)
                    +--+
                    GPIO
```

## Flow Control

Your task is to remove the first error caused by missing flow control.
More specifically, you are supposed to alter the data source such that new data values are sent only after the data has been consumed by the sink.
Document what problems you encountered and how you fixed them in your presentation.
Find and display an event where an erroneous data word is received (screenshot -> lab report).
Furthermore, observe the rate of erroneous data words at the receiver and compute the MTBU (measurement concept & results -> lab report) as well as the error probability (-> lab report) for a transmission.

## Synchronization

Change the design to also tackle the second error source and ultimately improve the MTBU to a value of at least $`10^6`$ years. 
For the presentation, prepare a block diagram and sketch the key concept behind your solution.
Can you sustain the data rate? If not determine the new maximum data rate (both throughput and min./max./avg. latency -> lab report).

