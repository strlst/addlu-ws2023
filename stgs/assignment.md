
# Asynchronous Control Circuits

In this lab exercise you will use the tool [Workcraft](https://www.workcraft.org) to design and synthesize asynchronous control circuits/state-machines from STG specifications and then implement and run them on an FPGA. We will use the Cyclone IV based DE2-115 board for that.

## LED Pattern Generator
The goal of the exercise is to implement four different, asynchronous, LED pattern generators. Your task is to specify and implement a pattern generator circuit for each of the following four patters a-d:

![Led Patterns](img/led_patterns.png)

Each controller shall implement following interface:

![Led Controller](img/led_controller.png)

**Important**: The controller circuits must implement a 2-phase asynchronous protocol with respect to their *ack* inputs. Therefore, the controllers must only switch to the next pattern when the *ack* input made a transition.

Note that you also have to design an appropriate (asynchronous) circuit for each controller that drives the *ack* input, based on the controller's *LED0-3* outputs. This logic will be different for each pattern and must be hazard-free. You can think of this component as a completion detector that monitors the LED outputs and toggels *ack* whenever the controller switched to a new pattern in the sequence.

## STGs and Workcraft

Before starting with the actual exercise we advise you to consult the Workcraft documentation:
Good introductory documents are the help page on [STGs](https://workcraft.org/help/stg/start) and the tutorial on how to model and verify a [C-gate](https://workcraft.org/tutorial/design/c_element/start).

**Remark**: Remember that an edge in an STG, e.g. $a+\rightarrow b+$, implies that there is a causal dependence between the rising edges of the signals *a* and *b*, i.e., the rising edge of *b* can only happen once the rising edge of *a* happened.


## FPGA Implementation
The provided quartus project targets the DE2-115 boards available in the TILab.
Use the switches *SW0* and *SW1* to select which controller drives the LEDs.

### C-Gates and Placement
You are provided with FPGA implementations of three 2-input C-gate versions in the `common/workcraft` directory:

- *C2*: Normal 2-input C gate with inputs *A* and *B* and output *Q*. No set or reset inputs.
- *C2\_r*: Resetable 2-input C gate. Asserting its input *R* will set the output to zero, regardless of the values of *A* and *B*.
- *C2\_s*: Setable 2-input C gate. Asserting its input *S* will set the output to one, regardless of the values of *A* and *B*.

The C-gates of this package are specifically targeted for the use on Cyclone IV FPGAs, such as the one on the DE2-115 board.
This FPGA features LUTs (lookup tables) with 4-inputs, thus each of the C-gates nicely fits into one such LUT.
For details on the internal architecture of the used FPGA, please refer to chapter 2 of its [handbook](https://cdrdv2-public.intel.com/653974/cyclone4-handbook.pdf).

Forcing the placement of certain elements of a design might help you to satisfy specific timing constraints.
To force the placement of a C-gate to some specific LE (logic element) in the FPGA, a design constraint similar to the following can be used:

> set_location_assignment LCCOMB_X53_Y58_N0 -to 
  "*|some_entity:some_instance|C2_r:C2_r_inst|C2_r_lut:C2_r_inst|Q_lut_output~1"

This constraint will place the C-gate, identified by the path in quotes, into the LUT of the first LE of the LAB (logic array block) identified by the *X* and *Y* coordinates 53 and 58. 
Such placement constraints can be added to the [SDC](lpg/quartus/lpg.sdc) file of your Quartus project.

You can use the Chip Planner in Quartus to view (and edit) the placement of your design. 

### Timeout Module
To slow down the operating speed of the circuit, use the provided *timeout* module in the `common` directory.
This (synchronous) module essentially behaves like a buffer with an adjustable delay, i.e., it preserves causality.
The clock signal is only required for the internal counter which is used to implement the delay.
Make sure that each step (i.e., line) of the LED patterns lasts at least one second.

### Reset
You will also have to devise a reset strategy for your circuits.
This means that all state-holding elements, i.e., the C-gates in your controllers, must be set to a defined state.
If the reset is active, the output of the controllers must switch to line 0 of the respective pattern.
By simulating the generated circuits in Workcraft, you can derive the initial values of all state-holding elements.

**Important**: Note that the reset functionality is **not** part of the STG specification (i.e. the reset signal does not appear in the STGs). Thus, design this functionality separately from the STGs.

## Working Remotely
The provided template contains support for working remotely on this lab assignment. This is achieved via the `dbg_port` module that is instantiated in [top.vhd](lpg/src/top.vhd).
You do not have to modify anything related to this module.
Just be aware that you have to use the internal `switches_int` and `keys_int` signals (instead of `switches` and `keys`), which allow emulating the interaction with the real switches and keys on the hardware remotely via a script.
If you use the board locally you don't have to change anything in the design, since the `dbg_port` module is essentially disabled after reset.
Hence, you can simply use the hardware switches.
In addition, there also exists a video stream to the remotely accessible boards.

To connect to the Remote Lab use the [rpa_shell.py](rpa_shell.py) script.
If you don't already know how to use it, please refer to the video tutorial on our TUWEL page.

First of all, copy the provided [remote.py](remote.py) script to the Remote Lab using the following command.

```
./rpa_shell.py --scp remote.py
```

Note that the home directory in the Remote Lab is the same as on the normal TILab computers.
The Remote Lab consists of special TILab hosts, that can only be accessed through the [rpa_shell.py](rpa_shell.py) script.

To run your locally compiled FPGA design run the following command: 

```
rpa_shell.py -p quartus/output_files/lpg.sof "./remote.py -i"
```

Additionally, you can pass the `-s target` command line argument to `rpa_shell.py` to open a live video stream of the board.

## Submission and Presentation
Use the template provided in the `lpg` directory for your solution.
Put the LED pattern generators in the files *lpg_{a,b,c,d}.vhd* in the `src` directory.
Furthermore, store your controllers' STGs as Workcraft files *led_controller_{a,b,c,d}.work* in the `workcraft` directory.

Prepare slides that summarize your results.
Include all STGs as well as the respective logic you designed to produce the *ack* signal.
Furthermore, include a figure showing how you put everything (controllers, *ack* logic, timeout etc.) together.

For all patterns, state whether they produce a CSC conflict in the respective controller's STG and if they don't, explain why this is the case!

**Do not forget to submit your source code and STGs via TUWEL!**
