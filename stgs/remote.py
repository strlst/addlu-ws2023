#!/bin/env python3

from time import sleep
import subprocess
from typing import List
from datetime import datetime
try:
	from dataclasses import dataclass
	from docopt import docopt
	import serial
	import curses
	from termcolor import colored, cprint
except:
	print("some required Python packages seem to not be installed!")
	print("run the following command to install them (as user, not as root!)")
	print("  pip3 install --user termcolor dataclasses docopt pyserial")
	exit(1)

class dbg_port:
	def __init__(self, port="/dev/ttyUSB0", baudrate=115200):
		self.uart = serial.Serial()
		self.uart.port = port
		self.uart.baudrate = baudrate
		self.uart.bytesize = serial.EIGHTBITS #number of bits per bytes
		self.uart.parity = serial.PARITY_NONE #set parity check: no parity
		self.uart.stopbits = serial.STOPBITS_ONE #number of stop bits
		self.uart.timeout = 1       # timeout in seconds
		self.uart.xonxoff = False
		self.uart.rtscts = False
		self.uart.dsrdtr = False

	def open(self):
		self.uart.open()
		cmd = b"i"
		self.uart.write(cmd) # reset interface

	def read_address(self, address):
		cmd = b"ir"+ format(address, 'x').encode("utf-8")+b"\n"
		self.uart.write(cmd)
		response = self.uart.readline().decode("utf-8").strip()
		if (response.lower() == "error"):
			raise Exception("Unable to read from adress " + str(address))
		return int(response.replace('\x00',''), 16)

	def write_address(self, address, value):
		cmd = b"w"
		cmd += format(address, 'x').encode("utf-8") + b" "
		cmd += format(value, 'x').encode("utf-8")
		cmd += b"\n"
		self.uart.write(cmd)
		response = self.uart.readline().decode("utf-8").strip().replace('\x00','')
		if (response.lower() != "ok"):
			raise Exception("ERROR writing " + value + " to address " + address + " (response: " + str(response) + ")")

	def get_id(self):
		try:
			return self.read_address(0)
		except Exception as ex:
			raise Exception(" Error while reading constant 'id' (" + str(ex) + ")" )
	def get_version(self):
		try:
			return self.read_address(1)
		except Exception as ex:
			raise Exception(" Error while reading constant 'version' (" + str(ex) + ")" )
	def get_sw_switches(self):
		try:
			return self.read_address(2)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_switches' (" + str(ex) + ")" )
	def set_sw_switches(self, value):
		try:
			self.write_address(2, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_switches' (" + str(ex) + ")" )
	def get_sw_keys(self):
		try:
			return self.read_address(3)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_keys' (" + str(ex) + ")" )
	def set_sw_keys(self, value):
		try:
			self.write_address(3, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_keys' (" + str(ex) + ")" )
	def get_actual_switches(self):
		try:
			return self.read_address(4)
		except Exception as ex:
			raise Exception(" Error while reading input 'actual_switches' (" + str(ex) + ")" )
	def get_actual_keys(self):
		try:
			return self.read_address(5)
		except Exception as ex:
			raise Exception(" Error while reading input 'actual_keys' (" + str(ex) + ")" )
	def get_ledr(self):
		try:
			return self.read_address(6)
		except Exception as ex:
			raise Exception(" Error while reading input 'ledr' (" + str(ex) + ")" )
	def get_ledg(self):
		try:
			return self.read_address(7)
		except Exception as ex:
			raise Exception(" Error while reading input 'ledg' (" + str(ex) + ")" )
	def get_hex(self):
		try:
			return self.read_address(8)
		except Exception as ex:
			raise Exception(" Error while reading input 'hex' (" + str(ex) + ")" )
	def get_swic(self):
		try:
			return self.read_address(9)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'swic' (" + str(ex) + ")" )
	def set_swic(self, value):
		try:
			self.write_address(9, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'swic' (" + str(ex) + ")" )
	def get_sw_res_n(self):
		try:
			return self.read_address(10)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_res_n' (" + str(ex) + ")" )
	def set_sw_res_n(self, value):
		try:
			self.write_address(10, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_res_n' (" + str(ex) + ")" )


def DrawLEDs(value, count, off_symbol="○", on_symbol="●", add_labels=False, label_idx_offset=0):
	lines = []
	line = ""
	off_on = [off_symbol, on_symbol]
	for i in reversed(range(0,count)):
		line += "[" + off_on[(value >> i) & 0x01] + "]"
	lines += [line]
	if(add_labels):
		lines += [ " ".join([str(i).rjust(2) for i in reversed(range(label_idx_offset,label_idx_offset+count))]) ]
	return lines

def DrawSwitches(value, count, add_labels = False):
	lines = ["┌─┐"*count]
	line1 = ""
	line2 = ""
	for i in reversed(range(0,count)):
		if((value >> i) & 0x01 == 1):
			line1 += "│█│"
			line2 += "│ │"
		else:
			line1 += "│ │"
			line2 += "│█│"
	lines += [line1, line2, "└─┘"*count]
	if(add_labels):
		lines += [ " ".join([str(i).rjust(2) for i in reversed(range(0,count))]) ]
	return lines

def DrawKeys(value, count, add_labels = False):
	lines = ["┌────┐"*count]
	line1 = ""
	line2 = ""
	labels = ""
	for i in reversed(range(0,count)):
		if((value >> i) & 0x01 == 0):
			#line1 += "│ ╔╗ │"
			#line2 += "│ ╚╝ │"
			line1 += "│ ▟▙ │"
			line2 += "│ ▜▛ │"
		else:
			line1 += "│ ╭╮ │"
			line2 += "│ ╰╯ │"
	lines += [line1, line2, "└────┘"*4]
	if(add_labels):
		lines += [" " +  "  ".join(["KEY"+str(i) for i in reversed(range(0,count))])]
	return lines

def DrawHex(values, hex_distance_left=0, add_labels=False, off_h = "-", on_h="━", off_v="╎", on_v="┃"):
	hex_template_small = """\
 00 
5  1
 66 
4  2
 33 \
"""
	def DrawSingleHexChar(c, label=None):
		output = hex_template_small
		for i in [0,6,3]:
			if ((c>>i) & 0x1):
				output = output.replace(str(i), off_h) 
			else:
				output = output.replace(str(i), on_h)
				
		for i in [1,2,4,5]:
			if ((c>>i) & 0x1):
				output = output.replace(str(i), off_v)
			else:
				output = output.replace(str(i), on_v)
		
		if(label != None):
			output += "\n" + label.center(4)
		
		return output
	
	if (isinstance(hex_distance_left, int)):
		hex_distance_left = [hex_distance_left]*len(values)
	
	if(add_labels):
		output_chars = [DrawSingleHexChar(values[i],label="HEX"+str(7-i)).split("\n") for i in range(0, len(values)) ]
	else:
		output_chars = [DrawSingleHexChar(values[i],label=None).split("\n") for i in range(0, len(values)) ]
	lines = []

	for line_idx in range(0,len(output_chars[0])):
		line = ""
		
		for char_idx in range(0,len(output_chars)):
			line += " "*hex_distance_left[char_idx] + output_chars[char_idx][line_idx] 
		
		lines += [line]
	
	return lines


@dataclass
class BoardIOState:
	Switches : int = 0
	Keys : int = 0
	Ledg : int = 0
	Ledr : int = 0
	Hex : List[int] = None
	
	def ResetSwitches(self):
		self.Switches = 0
	
	def ResetKeys(self):
		self.Keys = 0xf
	
	def ToggleSwitch(self, switch):
		self.Switches ^= (1<<switch)
	
	def ToggleKey(self, key):
		self.Keys ^= (1<<key)
	
	def ReadState(self, port, full=True):
		if (full == True):
			self.Switches = port.get_sw_switches()
			self.Keys = port.get_actual_keys()

		self.Ledg = port.get_ledg()
		self.Ledr = port.get_ledr()
		hex_value = port.get_hex()
		self.Hex = [ (hex_value >> i) & 0xff for i in reversed(range(0,64,8))]
	
	def WriteState(self, port):
		port.set_sw_switches(self.Switches)
		port.set_sw_keys(self.Keys)

	def DrawBoard(self, win, x, y, add_labels=False):
		def draw_lines(lines, win, x, y, red_symbols = [], green_symbols = [], gray_symbols=[]):
			win.move(y,x)
			for line in lines:
				for c in line:
					if (c in red_symbols):
						win.addstr(c, curses.A_BOLD | curses.color_pair(1))
					elif (c in green_symbols):
						win.addstr(c, curses.A_BOLD | curses.color_pair(2))
					elif (c in gray_symbols):
						win.addstr(c, curses.A_BOLD | curses.color_pair(3))
					else:
						win.addstr(c)
				y += 1
				win.move(y,x)
		
		hex_off_h = "-"
		hex_off_v = "|"
		hex_on_h = "━"
		hex_on_v = "┃"
		hex_symbols = DrawHex(
			self.Hex,
			hex_distance_left = [0,1,4,1,5,1,1,1], add_labels=add_labels, 
			on_h=hex_on_h, on_v=hex_on_v, off_h=hex_off_h, off_v=hex_off_v)
		red_leds = DrawLEDs(self.Ledr, 18, add_labels=add_labels)
		green_leds = DrawLEDs(self.Ledg, 8, add_labels=add_labels)
		switches = DrawSwitches(self.Switches, 18, add_labels=add_labels) 
		keys = DrawKeys(self.Keys, 4, add_labels=add_labels)
		ledg_8 = DrawLEDs((self.Ledg >> 8) & 0x1, 1, add_labels=add_labels, label_idx_offset=8)

		hex_height = len(hex_symbols)
		leds_height = len(green_leds)
		switches_height = len(switches)

		draw_lines(
			hex_symbols,
			win, x, y,
			red_symbols=[hex_on_h, hex_on_v],
			gray_symbols=[hex_off_h, hex_off_v])

		draw_lines(
			red_leds,
			win, x, y+hex_height,\
			red_symbols = ["●"])

		draw_lines(
			green_leds,
			win, 
			x+len(red_leds[0]),
			y+hex_height,
			green_symbols = ["●"])
		
		draw_lines(
			switches, 
			win, 
			x,
			y+hex_height+leds_height)
		
		draw_lines(
			keys,
			win,
			x+len(switches[0])+1,
			y+hex_height+leds_height)

		draw_lines(
			ledg_8,
			win, 
			x+23,
			y+2,
			green_symbols = ["●"])
		
		win.move(y+hex_height+leds_height+switches_height, 0)

switches_keymap = {
	ord('0'): 0,
	ord('1'): 1,
	ord('2'): 2,
	ord('3'): 3,
	ord('4'): 4,
	ord('5'): 5,
	ord('6'): 6,
	ord('7'): 7,
	ord('8'): 8,
	ord('9'): 9,
	ord('q'): 10,
	ord('w'): 11,
	ord('e'): 12,
	ord('r'): 13,
	ord('t'): 14,
	ord('y'): 15,
	ord('u'): 16,
	ord('i'): 17,
}

keys_keymap = {
	ord('a'): 3,
	ord('s'): 2,
	ord('d'): 1,
	ord('f'): 0
}


help_text_template = """
Commands: 
 > v:  Show labels for board IO.
 > m:  Switch between between the persistent and non-persistent input mode.
       In the persistent input mode pressed keys/switches must be reset manually
       by pressing the same key again. In the non-persistent mode keys are reset
       automatically after some short time.
 > h:  Show/hide this help screen.
"""
help_text = None
def HelpScreen():
	def GetKeyString(key):
		if (key > 255):
			special_keys = {
				curses.KEY_UP    : "↑",
				curses.KEY_DOWN  : "↓",
				curses.KEY_LEFT  : "←",
				curses.KEY_RIGHT : "→",
				curses.KEY_END   : "End",
				curses.KEY_NPAGE : "Page Up",
				curses.KEY_PPAGE : "Page Down"
			}
			return special_keys[key]
		else:
			return chr(key)
	
	def GetKeybyValue(d, value):
		for key, val in d.items():
			if (val == value):
				return key
		raise Exception("Value '" + str(value) + "' not found")

	global help_text
	if (help_text == None):
		help_text = help_text_template
		
		help_text += "\n"
		help_text += "Key mappings for inputs devices:\n"
		help_text += " > Switches: SW18,...,SW0: "
		help_text += ", ".join([GetKeyString(GetKeybyValue(switches_keymap, i)) for i in reversed(range(0,18))])
		help_text += "\n"
		help_text += " > Keys: KEY3,...,KEY0: "
		help_text += ", ".join([GetKeyString(GetKeybyValue(keys_keymap, i)) for i in reversed(range(0,4))])
		help_text += "\n"
	return help_text

def InteractiveMode(stdscr, port):
	def print_header(text):
		stdscr.addstr(text)
		if (sticky_mode):
			stdscr.addstr(" >> non-persistent mode <<")
		stdscr.addstr("\n")

	curses.curs_set(0)
	curses.start_color()
	curses.init_color(15, 500, 500, 500) # a gray color
	curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
	curses.init_pair(3, 15, curses.COLOR_BLACK)
	stdscr.clear()
	stdscr.refresh()
	stdscr.nodelay(True) # non-blocking getch
		
	#check the bounds of the window
	height, width = stdscr.getmaxyx()
	if(height < 30):
		raise Exception("Error: Increase the height of your terminal! (min. 25 lines)")
	if(width < 80):
		raise Exception("Error: Increase the width of your terminal! (min. 80 columns)")
	
	stop = False
	sticky_mode = True
	show_help = False
	show_board_io_lables = False
	
	board_state = BoardIOState()
	board_state.ReadState(port, True)
	
	pressed_keys = {}
	
	while (not stop):
		now = datetime.now()

		if(sticky_mode):
			pressed_keys = {}

		while(True):
			c = stdscr.getch()
			if (c == curses.KEY_END or c == 27):
				stop = True
				break
			elif (c == -1):
				break
			elif (c == ord('h')):
				show_help = not show_help
			
			if(not show_help):
				if (c == ord('v')):
					show_board_io_lables = not show_board_io_lables
				elif (c == ord('m')):
					sticky_mode = not sticky_mode
				
				pressed_keys[c] = now
		
		keys_to_delete = []
		for key, time in pressed_keys.items():
			if ((now - time).total_seconds() > 0.25):
				keys_to_delete.append(key)
		for key in keys_to_delete:
			del pressed_keys[key]
		
		if(not sticky_mode):
			board_state.ResetSwitches()
			board_state.ResetKeys()
		
		for c in pressed_keys.keys():
			if (c in switches_keymap.keys()):
				board_state.ToggleSwitch(switches_keymap[c])
		
		for c in pressed_keys.keys():
			if (c in keys_keymap.keys()):
				board_state.ToggleKey(keys_keymap[c])
		
		board_state.WriteState(port)
			
		stdscr.clear()
		if(show_help):
			stdscr.addstr(HelpScreen())
			curses.napms(100)
		else:
			board_state.ReadState(port, False)
			print_header("Board IO")
			board_state.DrawBoard(stdscr, 0, 1, add_labels=show_board_io_lables)
			stdscr.addstr("\n")
			stdscr.addstr("Press h to show the help screen and End/ESC to quit\n")

usage_msg = """
A simple tool to communicate with the dbg_port of the VHDL design.

Examples:
  Read the red LEDs:
   > remote.py -r
  Get the state of the red LED 3
   > remote.py -r -b 3 
  Get the state of the red LEDs 4 to 7
   > remote.py -r -b 7:4
  
Usage:
  remote.py (-k | -s) ([-b B] <VALUE> | [-b B | -a])
  remote.py (-g | -r) [-a | -b B]
  remote.py -x [-a]
  remote.py [--swic] [<VALUE>]
  remote.py -i [--reset]
  remote.py -p SOF_FILE
  remote.py --reset | -h | -v

Options:
  -h --help     Show this help screen
  -v --version  Show version information
  -i            Interactive Mode (if the --reset flag is set, the design will
                be reset before the interactive mode is started)
  --reset       ssue a software reset (this deasserts sw_reset_n for a
                sufficently long time span to reset the design)
  -k            Access the push buttons (4 bit)
  -s            Access the switches (18 bit)
  -g            Access the green LEDs (9 bit)
  -r            Access the red LEDs (18 bit)
  -x            Access the hex display (8 x 7 bit)
  --swic        Access the Software Input Control flag to select whether the 
                boards physical switches and buttons should be used (VALUE = 0)
                or the virtual software input set via -s and -k (VALUE = 1).
                Setting values using -k or -s automatically sets this flag.
  -b B          Select individual bits or bit ranges (a:b)
  -a            Use ASCII art to present the results of a read access.
  -p SOF_FILE   Download SOF_FILE file to the FPGA board
"""
#--------|---------|---------|---------|---------|---------|---------|---------|

def SWReset(port):
	port.set_sw_reset_n(0)
	sleep(0.25)
	port.set_sw_reset_n(1)

def ParseValue(value):
	x = 0
	for v in value.split("|"):
		v = v.strip()
		if(v.startswith("'") and v.endswith("'") and len(v) == 3):
			x |= ord(v[1])
		else:
			x |= int(v,0)
	return x

def PrintValue(value, bit_selection=None):
	if (bit_selection == None):
		print(hex(value))
	else:
		bit_selection = bit_selection.split(":")
		if (len(bit_selection)==1):
			print(hex((value>>int(bit_selection[0],0) & 0x1)))
		elif (len(bit_selection)==2):
			a = int(bit_selection[0],0)
			b = int(bit_selection[1],0)
			if(a < b):
				print("invalid range specification")
				return
			print( hex( (~((-1)<<(a+1)) & value) >> b) )
		else:
			print("invalid bit selection")

def RMW(old_value, new_value, bit_selection):
	bit_selection = bit_selection.split(':')
	if (len(bit_selection)==1):
		bit_selection = int(bit_selection[0],0)
		if( not (new_value == 0 or new_value == 1)):
			print("invalid value specified " + str(new_value))
			return None
		return (old_value &  ~(1<<bit_selection)) | (new_value<<bit_selection) 

	elif (len(bit_selection)==2):
		a = int(bit_selection[0],0)
		b = int(bit_selection[1],0)
		if(a < b):
			print("invalid range specification")
			return
			
		mask = ((2**((a-b)+1)-1))
		
		if(~mask & new_value != 0):
			print("invalid value for this this range: " + str(hex(new_value)))
			return None
		
		return (old_value & ~(mask << b) | (new_value << b))
	else:
		print("invalid bit selection")
		return None

def PrintChannelState(req, ack, data, sw = 0):
	data_str = "transitioning" if sw else  hex(data) if req else "spacer"
	print(f"data: {data_str}" )
	print(f"ack: {ack}")

def main():
	options = docopt(usage_msg, version="1.0")
	#print(options)
	
	if (options["-p"] != None):
		#if there is still an active jtag UART connection programming will fail, hence kill the nios2-terminal first
		subprocess.run(["killall", "nios2-terminal"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		subprocess.run(["quartus_pgm", "-m", "jtag" ,"-o" , "p;" + options["-p"] ])
		return
	
	port = dbg_port()
	port.open()
	if (port.get_id() != 0x0add23e3):
		print("Invalid debug core ID!")
		exit()
	if (port.get_version() != 1):
		print("Incompatible debug core version (Update your dbg_core.qxp file!)")
		exit()
	
	value = options["<VALUE>"]
	if (value != None):
		value = ParseValue(options["<VALUE>"])
	
	if(options["--reset"]):
		SWReset(port)
	
	if(options["-i"]):
		curses.wrapper(InteractiveMode, port)
	elif(options["-g"]): #green LEDs
		if (options["-a"]):
			output = "\n".join(DrawLEDs(port.get_ledg(), 9, add_labels=True,on_symbol="●")).replace("●", colored("●", 'green'))
			print(output)
		else:
			PrintValue(port.get_ledg(), options["-b"])
	elif(options["-r"]): #red LEDs
		if (options["-a"]):
			output = "\n".join(DrawLEDs(port.get_ledr(), 18, add_labels=True,on_symbol="●")).replace("●", colored("●", 'red'))
			print(output)
		else:
			PrintValue(port.get_ledr(), options["-b"])
	elif(options["-x"]): #hex displays
		hex_value = port.get_hex()
		hex_values = [ (hex_value >> i) & 0xff for i in reversed(range(0,64,8))]
		if (options["-a"]):
			output = "\n".join(DrawHex(
				hex_values, hex_distance_left=1,
				add_labels=True,
				on_h="━", on_v="┃")).replace("━", colored("━", 'red')).replace("┃", colored("┃", 'red'))
			print(output)
		else:
			print(" ".join([hex(i) for i in hex_values]))
	elif(options["-s"]):
		if(value != None):
			if(options["-b"] != None):
				value = RMW(port.get_actual_switches(), value, options["-b"])
			
			if(value == None):
				exit(1)
			port.set_sw_switches(value)
		else:
			if (options["-a"]):
				output = "\n".join(DrawSwitches(port.get_actual_switches(), 18, add_labels=True))
				print(output)
			else:
				PrintValue(port.get_actual_switches(), options["-b"])
	elif(options["-k"]):
		if(value != None):
			if(options["-b"] != None):
				value = RMW(port.get_actual_keys(), value, options["-b"])
			
			if(value == None):
				exit(1)
			port.set_sw_keys(value)
		else:
			if (options["-a"]):
				output = "\n".join(DrawKeys(port.get_actual_keys(), 4, add_labels=True))
				print(output)
			else:
				PrintValue(port.get_actual_keys(), options["-b"])
	elif (options["--swic"]):
		if (options["<VALUE>"] != None):
			port.set_swic(int(options["<VALUE>"], 0))
		else:
			print(hex(port.get_swic()))



if(__name__ == "__main__"):
	main()

