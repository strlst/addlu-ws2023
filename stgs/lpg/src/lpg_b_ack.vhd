library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;

entity lpg_b_ack is
	port (
		res_n : in std_logic;
		ack : out std_logic;
		leds : in std_logic_vector(3 downto 0)
	);
end entity;

architecture arch of lpg_b_ack is
	component lpg_b_signals_ack
	port (
		l0 : in std_logic;
		l1 : in std_logic;
		l2 : in std_logic;
		l3 : in std_logic;
		ack : in std_logic;
		ack_SET : out std_logic;
		ack_nRESET : out std_logic
	);
	end component;

	signal a_ack : std_logic;
	signal ack_set : std_logic;
	signal ack_nreset : std_logic;
begin
	ack <= a_ack;

	lpg_b_signals_ack_inst : component lpg_b_signals_ack
	port map (
		l0 => leds(0),
		l1 => leds(1),
		l2 => leds(2),
		l3 => leds(3),
		ack => a_ack,
		ack_SET => ack_set,
		ack_nRESET => ack_nreset
	);

	ack_inst : entity work.C2_s
	port map (
		A => ack_set,
		B => ack_nreset,
		Q => a_ack,
		S => not res_n
	);

end architecture;
