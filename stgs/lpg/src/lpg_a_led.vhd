library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;

entity lpg_a_led is
	port (
		res_n : in std_logic;
		ack : in std_logic;
		leds : out std_logic_vector(3 downto 0)
	);
end entity;

architecture arch of lpg_a_led is

	component lpg_a_signals_led
	port (
		ack : in std_logic;
		l0 : in std_logic;
		l1 : in std_logic;
		l2 : in std_logic;
		l3 : in std_logic;
		csc : in std_logic;
		csc1 : in std_logic;
		csc2 : in std_logic;
		csc3 : in std_logic;
		csc4 : in std_logic;
		csc5 : in std_logic;
		l0_SET : out std_logic;
		l0_nRESET : out std_logic;
		l1_SET : out std_logic;
		l1_nRESET : out std_logic;
		l2_SET : out std_logic;
		l2_nRESET : out std_logic;
		l3_SET : out std_logic;
		l3_nRESET : out std_logic;
		csc_SET : out std_logic;
		csc_nRESET : out std_logic;
		csc1_SET : out std_logic;
		csc1_nRESET : out std_logic;
		csc2_SET : out std_logic;
		csc2_nRESET : out std_logic;
		csc3_SET : out std_logic;
		csc3_nRESET : out std_logic;
		csc4_SET : out std_logic;
		csc4_nRESET : out std_logic;
		csc5_SET : out std_logic;
		csc5_nRESET : out std_logic
	);
	end component;

	signal csc : std_logic_vector(5 downto 0);
	signal csc_set : std_logic_vector(5 downto 0);
	signal csc_nreset : std_logic_vector(5 downto 0);

	signal a_leds : std_logic_vector(3 downto 0);
	signal a_leds_set : std_logic_vector(3 downto 0);
	signal a_leds_nreset : std_logic_vector(3 downto 0);
begin

	leds <= a_leds;

	lpg_a_signals_led_inst : component lpg_a_signals_led
	port map (
		ack => ack,
		l0 => a_leds(0),
		l1 => a_leds(1),
		l2 => a_leds(2),
		l3 => a_leds(3),
		csc => csc(0),
		csc1 => csc(1),
		csc2 => csc(2),
		csc3 => csc(3),
		csc4 => csc(4),
		csc5 => csc(5),
		l0_SET => a_leds_set(0),
		l0_nRESET => a_leds_nreset(0),
		l1_SET => a_leds_set(1),
		l1_nRESET => a_leds_nreset(1),
		l2_SET => a_leds_set(2),
		l2_nRESET => a_leds_nreset(2),
		l3_SET => a_leds_set(3),
		l3_nRESET => a_leds_nreset(3),
		csc_SET => csc_set(0),
		csc_nRESET => csc_nreset(0),
		csc1_SET => csc_set(1),
		csc1_nRESET => csc_nreset(1),
		csc2_SET => csc_set(2),
		csc2_nRESET => csc_nreset(2),
		csc3_SET => csc_set(3),
		csc3_nRESET => csc_nreset(3),
		csc4_SET => csc_set(4),
		csc4_nRESET => csc_nreset(4),
		csc5_SET => csc_set(5),
		csc5_nRESET => csc_nreset(5)
	);

	-- all 0 c-gates
	a_led_gen : for i in 0 to 3 generate
		a_led_inst : entity work.C2_r
		port map (
			A => a_leds_set(i),
			B => a_leds_nreset(i),
			Q => a_leds(i),
			R => not res_n
		);
	end generate;

	-- all 0 c-gates
	csc_gen : for i in 0 to 5 generate
		csc_inst : entity work.C2_r
		port map (
			A => csc_set(i),
			B => csc_nreset(i),
			Q => csc(i),
			R => not res_n
		);
	end generate;

end architecture;
