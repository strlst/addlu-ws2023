// Verilog netlist generated by Workcraft 3
// * Workcraft version:  3.4.1
// * Operating system:   Linux amd64
// * Creation timestamp: 2024-01-18T15:48:51.821+01:00
// * Design title:       lpg_c_signals_ack
// * JavaScript command: framework.exportWork(workspaceEntry, 'lpg_c_signals_ack.v', 'Verilog')
module lpg_c_signals_ack (l0, l1, l2, l3, ack_SET, ack_nRESET);
    input l0, l1, l2, l3;
    output ack_SET, ack_nRESET;

    assign ack_SET = ~l0 & ~l1 & ~l2 & ~l3;
    assign ack_nRESET = (~l0 & ~l1 & ~l2 | ~l3) & (~l0 | ~l2);
    //assign ack = ack_SET & ack_nRESET | ack & (~~(ack_SET | ack_nRESET)); c-gate

    // signal values at the initial state:
    // !ack ack_SET ack_nRESET !l0 !l1 !l2 !l3
endmodule
