library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;

entity lpg_d_led is
	port (
		res_n : in std_logic;
		ack : in std_logic;
		leds : out std_logic_vector(3 downto 0)
	);
end entity;

architecture arch of lpg_d_led is

	component lpg_d_signals_led
	port (
		ack : in std_logic;
		l0 : in std_logic;
		l1 : in std_logic;
		l2 : in std_logic;
		l3 : in std_logic;
		l0_SET : out std_logic;
		l0_nRESET : out std_logic;
		l1_SET : out std_logic;
		l1_nRESET : out std_logic;
		l2_SET : out std_logic;
		l2_nRESET : out std_logic;
		l3_SET : out std_logic;
		l3_nRESET : out std_logic
	);
	end component;

	signal a_leds : std_logic_vector(3 downto 0);
	signal a_leds_set : std_logic_vector(3 downto 0);
	signal a_leds_nreset : std_logic_vector(3 downto 0);
begin

	leds <= a_leds;

	lpg_d_signals_led_inst : component lpg_d_signals_led
	port map (
		ack => ack,
		l0 => a_leds(0),
		l1 => a_leds(1),
		l2 => a_leds(2),
		l3 => a_leds(3),
		l0_SET => a_leds_set(0),
		l0_nRESET => a_leds_nreset(0),
		l1_SET => a_leds_set(1),
		l1_nRESET => a_leds_nreset(1),
		l2_SET => a_leds_set(2),
		l2_nRESET => a_leds_nreset(2),
		l3_SET => a_leds_set(3),
		l3_nRESET => a_leds_nreset(3)
	);

	-- all 0 c-gates
	a_led_r_gen : for i in 0 to 1 generate
		a_led_r_inst : entity work.C2_r
		port map (
			A => a_leds_set(i),
			B => a_leds_nreset(i),
			Q => a_leds(i),
			R => not res_n
		);
	end generate;

	-- all 1 c-gates
	a_led_s_gen : for i in 2 to 3 generate
		a_led_s_inst : entity work.C2_s
		port map (
			A => a_leds_set(i),
			B => a_leds_nreset(i),
			Q => a_leds(i),
			S => not res_n
		);
	end generate;

end architecture;
