library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;
use work.timeout_pkg.all;

entity top is
	port (
		clk : in std_logic;
		switches : in std_logic_vector(17 downto 0);
		keys: in std_logic_vector(3 downto 0);
		ledr : out std_logic_vector(17 downto 0);

		-- if you need something for debugging
		ledg : out std_logic_vector(8 downto 0);
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0);

		-- Used by the dbg port
		rx : in std_logic;
		tx : out std_logic
	);
end entity;

architecture arch of top is
	signal timeout_req, timeout_ack : std_logic;
	signal leds_a, leds_b, leds_c, leds_d : std_logic_vector(3 downto 0);
	signal res_n, sw_res_n : std_logic;

	-- Use these signals instead of the non-internal ones
	-- The internal signals can be remotely controlled
	-- However, note that keys(0) is already used as reset
	signal switches_int : std_logic_vector(17 downto 0);
	signal keys_int : std_logic_vector(3 downto 0);
	
	
	constant CHAR_OFF  : std_logic_vector(6 downto 0) := "1111111";
	constant CHAR_DASH : std_logic_vector(6 downto 0) := "0111111";
	constant CHAR_H    : std_logic_vector(6 downto 0) := "0001001";
	constant CHAR_O    : std_logic_vector(6 downto 0) := "1000000";
	constant CHAR_E    : std_logic_vector(6 downto 0) := "0000110";
	constant CHAR_L    : std_logic_vector(6 downto 0) := "1000111";
	
	constant CHAR_OPENING_BRACKET : std_logic_vector(6 downto 0) := "1000110";
	constant CHAR_CLOSING_BRACKET : std_logic_vector(6 downto 0) := "1110000";
begin
	res_n <= keys_int(0) and sw_res_n;

	lpg_a_inst : entity work.lpg_a
	port map (
		clk   => clk,
		res_n => res_n,
		leds  => leds_a
	);
	
	lpg_b_inst : entity work.lpg_b
	port map (
		clk   => clk,
		res_n => res_n,
		leds  => leds_b
	);
	
	lpg_c_inst : entity work.lpg_c
	port map (
		clk   => clk,
		res_n => res_n,
		leds  => leds_c
	);
	
	lpg_d_inst : entity work.lpg_d
	port map (
		clk   => clk,
		res_n => res_n,
		leds  => leds_d
	);
	
	process(all)
	begin
		ledr <= (others=>'0');
		case switches_int(1 downto 0) is
			when "00" => ledr(3 downto 0) <= leds_a;
			when "01" => ledr(3 downto 0) <= leds_b;
			when "10" => ledr(3 downto 0) <= leds_c;
			when "11" => ledr(3 downto 0) <= leds_d;
			when others => ledr(3 downto 0) <= (others=>'0');
		end case;
		
	end process;

	ledg <= "101010101";

	hex7 <= CHAR_H;
	hex6 <= CHAR_E;
	hex5 <= CHAR_L;
	hex4 <= CHAR_L;
	hex3 <= CHAR_O;
	hex2 <= CHAR_OFF;
	hex1 <= CHAR_OFF;
	hex0 <= CHAR_OFF;

	dbg: block
		signal dbg_ledg : std_logic_vector(ledg'range);
		signal dbg_ledr : std_logic_vector(ledr'range);

		signal dbg_hex0 : std_logic_vector(6 downto 0);
		signal dbg_hex1 : std_logic_vector(6 downto 0);
		signal dbg_hex2 : std_logic_vector(6 downto 0);
		signal dbg_hex3 : std_logic_vector(6 downto 0);
		signal dbg_hex4 : std_logic_vector(6 downto 0);
		signal dbg_hex5 : std_logic_vector(6 downto 0);
		signal dbg_hex6 : std_logic_vector(6 downto 0);
		signal dbg_hex7 : std_logic_vector(6 downto 0);
	begin

		dbg_ledg <= ledg;
		dbg_ledr <= ledr;

		dbg_hex0 <= hex0;
		dbg_hex1 <= hex1;
		dbg_hex2 <= hex2;
		dbg_hex3 <= hex3;
		dbg_hex4 <= hex4;
		dbg_hex5 <= hex5;
		dbg_hex6 <= hex6;
		dbg_hex7 <= hex7;

		dbg_port_inst : entity work.dbg_port
		port map(
			clk         => clk,
			res_n       => keys(0),
			rx          => rx,
			tx          => tx,
			ledr        => dbg_ledr,
			ledg        => dbg_ledg,
			sw_res_n    => sw_res_n,
			hex0        => dbg_hex0,
			hex1        => dbg_hex1,
			hex2        => dbg_hex2,
			hex3        => dbg_hex3,
			hex4        => dbg_hex4,
			hex5        => dbg_hex5,
			hex6        => dbg_hex6,
			hex7        => dbg_hex7,
			hw_switches => switches,
			hw_keys     => keys,
			switches    => switches_int,
			keys        => keys_int
		);
	end block;
end architecture;
