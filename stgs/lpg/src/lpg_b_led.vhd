library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;

entity lpg_b_led is
	port (
		res_n : in std_logic;
		ack : in std_logic;
		leds : out std_logic_vector(3 downto 0)
	);
end entity;

architecture arch of lpg_b_led is

	component lpg_b_signals_led
	port (
		ack : in std_logic;
		l0 : in std_logic;
		l1 : in std_logic;
		l2 : in std_logic;
		l3 : in std_logic;
		csc : in std_logic;
		csc1 : in std_logic;
		l0_SET : out std_logic;
		l0_nRESET : out std_logic;
		l1_SET : out std_logic;
		l1_nRESET : out std_logic;
		l2_SET : out std_logic;
		l2_nRESET : out std_logic;
		l3_SET : out std_logic;
		l3_nRESET : out std_logic;
		csc_SET : out std_logic;
		csc_nRESET : out std_logic;
		csc1_SET : out std_logic;
		csc1_nRESET : out std_logic
	);
	end component;

	signal csc : std_logic_vector(1 downto 0);
	signal csc_set : std_logic_vector(1 downto 0);
	signal csc_nreset : std_logic_vector(1 downto 0);

	signal a_leds : std_logic_vector(3 downto 0);
	signal a_leds_set : std_logic_vector(3 downto 0);
	signal a_leds_nreset : std_logic_vector(3 downto 0);
begin

	leds <= a_leds;

	csc(0) <= ((not ack or not a_leds(1)) and csc(0)) or (ack and a_leds(0));
	csc(1) <= ((not ack or not a_leds(3)) and csc(1)) or (ack and a_leds(2));

	lpg_b_signals_led_inst : component lpg_b_signals_led
	port map (
		ack => ack,
		l0 => a_leds(0),
		l1 => a_leds(1),
		l2 => a_leds(2),
		l3 => a_leds(3),
		csc => csc(0),
		csc1 => csc(1),
		l0_SET => a_leds_set(0),
		l0_nRESET => a_leds_nreset(0),
		l1_SET => a_leds_set(1),
		l1_nRESET => a_leds_nreset(1),
		l2_SET => a_leds_set(2),
		l2_nRESET => a_leds_nreset(2),
		l3_SET => a_leds_set(3),
		l3_nRESET => a_leds_nreset(3),
		csc_SET => csc_set(0),
		csc_nRESET => csc_nreset(0),
		csc1_SET => csc_set(1),
		csc1_nRESET => csc_nreset(1)
	);

	-- 0 c-gate
	aled_inst_0 : entity work.C2_r
	port map (
		A => a_leds_set(0),
		B => a_leds_nreset(0),
		Q => a_leds(0),
		R => not res_n
	);

	-- 0 c-gate
	aled_inst_1 : entity work.C2_r
	port map (
		A => a_leds_set(1),
		B => a_leds_nreset(1),
		Q => a_leds(1),
		R => not res_n
	);

	-- 1 c-gate
	aled_inst_2 : entity work.C2_s
	port map (
		A => a_leds_set(2),
		B => a_leds_nreset(2),
		Q => a_leds(2),
		S => not res_n
	);

	-- 0 c-gate
	aled_inst_3 : entity work.C2_r
	port map (
		A => a_leds_set(3),
		B => a_leds_nreset(3),
		Q => a_leds(3),
		R => not res_n
	);

end architecture;
