library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;
use work.timeout_pkg.all;

entity lpg_c is
	generic (
		CLK_FREQ : integer := 50000000;
		TIMEOUT_MS : integer := 1000;
		SYNC_STAGES : integer := 2
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;
		leds : out std_logic_vector(3 downto 0)
	);
end entity;

architecture arch of lpg_c is
	component lpg_c_led is
	port (
		res_n : in std_logic;
		ack : in std_logic;
		leds : out std_logic_vector(3 downto 0)
	);
	end component;

	component lpg_c_ack is
	port (
		res_n : in std_logic;
		ack : out std_logic;
		leds : in std_logic_vector(3 downto 0)
	);
	end component;

	signal timeout_req, timeout_ack : std_logic;
	signal a_leds : std_logic_vector(3 downto 0) := "0000";
begin

	timeout_inst : timeout
	generic map (
		CLK_FREQ    => CLK_FREQ,
		TIMEOUT_MS  => TIMEOUT_MS,
		SYNC_STAGES => SYNC_STAGES
	)
	port map (
		clk   => clk,
		res_n => res_n,
		req   => timeout_req,
		ack   => timeout_ack
	);
	
	--remove the following two lines and replace them with your LED controller and the logic to generate its ack input
	--timeout_req <= not timeout_ack;
	--leds <= (0=>timeout_req, 1=>timeout_ack, 2=>'0', 3=>'0');

	lpg_c_led_inst : component lpg_c_led
	port map (
		res_n => res_n,
		ack => timeout_ack,
		leds => a_leds
	);

	lpg_c_ack_inst : component lpg_c_ack
	port map (
		res_n => res_n,
		ack => timeout_req,
		leds => a_leds
	);

	leds <= a_leds;

end architecture;
