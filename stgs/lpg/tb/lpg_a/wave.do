onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb/stop
add wave -noupdate -radix hexadecimal /tb/dut/*
add wave -noupdate -radix hexadecimal /tb/dut/lpg_a_led_inst/*
add wave -noupdate -radix hexadecimal /tb/dut/lpg_a_ack_inst/*
add wave -noupdate -radix hexadecimal /tb/dut/timeout_inst/*
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
WaveRestoreZoom {0 ps} {220 ns}
update
