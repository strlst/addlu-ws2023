library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.workcraft_pkg.all;
use work.timeout_pkg.all;

entity tb is
end entity;

architecture dut of tb is
	constant CLK_PERIOD : time := 1 ns;
	signal clk : std_logic;
	signal res_n : std_logic;
	signal leds : std_logic_vector(3 downto 0);
	signal stop : boolean := false;
begin

	dut : entity work.lpg_b
	generic map (
		CLK_FREQ => 1000,
		TIMEOUT_MS => 2,
		SYNC_STAGES => 1
	) port map (
		clk => clk,
		res_n => res_n,
		leds => leds
	);

	stimulus : process
	begin
		report "starting sim";
		wait for 300*CLK_PERIOD;
		stop <= true;
		report "stopping sim";
		wait;
	end process;

	generate_clk : process
	begin
		while not stop loop
			clk <= '1', '0' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

	generate_reset : process
	begin
		res_n <= '0';
		wait for 2*CLK_PERIOD;
		res_n <= '1';
		wait;
	end process;

end architecture;
