vlib work
vmap work work

vcom -work work -2008 ../../../common/math/math_pkg.vhd
vcom -work work -2008 ../../../common/workcraft/workcraft_pkg.vhd
vcom -work work -2008 ../../../common/workcraft/C2_r.vhd
vcom -work work -2008 ../../../common/workcraft/C2_r_lut.vhd
vcom -work work -2008 ../../../common/workcraft/C2_s.vhd
vcom -work work -2008 ../../../common/workcraft/C2_s_lut.vhd
vcom -work work -2008 ../../../common/timeout/timeout_pkg.vhd
vcom -work work -2008 ../../../common/timeout/timeout.vhd
vlog -work work ../../src/lpg_b_signals_led.v
vlog -work work ../../src/lpg_b_signals_ack.v
vcom -work work -2008 ../../src/lpg_b_led.vhd
vcom -work work -2008 ../../src/lpg_b_ack.vhd
vcom -work work -2008 ../../src/lpg_b.vhd

vcom -work work -2008 tb.vhd
