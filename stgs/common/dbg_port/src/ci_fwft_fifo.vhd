
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ci_math_pkg.all;
use work.ci_ram_pkg.all;

entity ci_fwft_fifo is
	generic (
		MIN_DEPTH  : integer;
		DATA_WIDTH : integer
	);
	port (
		clk       : in  std_logic;
		res_n     : in  std_logic;

		rd_data   : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		rd_ack    : in  std_logic;
		rd_valid  : out std_logic;

		wr_data   : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		wr        : in  std_logic;
		full      : out std_logic
	);
end entity;


architecture arch of ci_fwft_fifo is
	signal rd, empty, not_empty : std_logic;
begin

	ci_fifo_inst : ci_fifo
	generic map (
		MIN_DEPTH          => MIN_DEPTH,
		DATA_WIDTH         => DATA_WIDTH,
		FILL_LEVEL_COUNTER => "OFF"
	)
	port map (
		clk        => clk,
		res_n      => res_n,
		rd_data    => rd_data,
		rd         => rd,
		wr_data    => wr_data,
		wr         => wr,
		empty      => empty,
		full       => full,
		fill_level => open
	);

	not_empty <= not empty;
	rd <= rd_ack or (not_empty and not rd_valid);
	
	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			rd_valid <= '0';
		elsif (rising_edge(clk)) then
			if (rd = '1') then
				rd_valid <= not_empty;
			end if;
		end if;
	end process;


end architecture;


