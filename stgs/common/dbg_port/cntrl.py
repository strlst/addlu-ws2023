#!/bin/python3

from docopt import docopt
import serial
import time

usage_msg = """
Usage:
  cntrl.py [options] <INTERFACE> [VALUE ... ]

Options:
  -d DEVICE      The UART device to use [default: /dev/ttyUSB0]
  -b BAUD_RATE   The baudrate of the UART interface [default: 115200]
  -c             Check if there is data to read or a free space to write data in
                 a FIFO interface.
  -h             Show this screen.
"""


interface_table = {"id": {"address": 0, "type": "constant"}, "version": {"address": 1, "type": "constant"}, "sw_switches": {"address": 2, "type": "output"}, "sw_keys": {"address": 3, "type": "output"}, "actual_switches": {"address": 4, "type": "input"}, "actual_keys": {"address": 5, "type": "input"}, "ledr": {"address": 6, "type": "input"}, "ledg": {"address": 7, "type": "input"}, "hex": {"address": 8, "type": "input"}, "swic": {"address": 9, "type": "output"}, "sw_res_n": {"address": 10, "type": "output"}}


def main():
	options = docopt(usage_msg, version="0.1")
	#print(options)

	uart = serial.Serial()
	uart.port = options["-d"]
	uart.baudrate = int(options["-b"])
	uart.bytesize = serial.EIGHTBITS #number of bits per bytes
	uart.parity = serial.PARITY_NONE #set parity check: no parity
	uart.stopbits = serial.STOPBITS_ONE #number of stop bits
	uart.timeout = 1       # timeout in seconds
	uart.xonxoff = False
	uart.rtscts = False
	uart.dsrdtr = False

	uart.open()

	if(options["<INTERFACE>"] not in interface_table):
		print("Unknown Interface!")
		exit(1)

	if(len(options["VALUE"]) == 0):
		if(options["-c"]):
			if(interface_table[options["<INTERFACE>"]]["type"] not in {"output_fifo", "input_fifo"}):
				print("The -c option can only be used with FIFO interfaces!")
				exit(1)
			cmd = b"ir"+ format(interface_table[options["<INTERFACE>"]]["address"]+1, 'x').encode("utf-8")+b"\n"
		else:
			cmd = b"ir"+ format(interface_table[options["<INTERFACE>"]]["address"], 'x').encode("utf-8")+b"\n"
		uart.write(cmd)
		response = uart.readline().decode("utf-8").strip()
		if (response.lower() == "error"):
			print("ERROR reading interface " + options["<INTERFACE>"])
			exit(1)
		rval_int = int(response, 16)
	
		try:
			rval = eval(interface_table[options["<INTERFACE>"]]['scale'])(rval_int)
		except KeyError:
			rval = rval_int

		try:
			print(interface_table[options["<INTERFACE>"]]['fmt'].format(rval))
		except KeyError:
			print("0x{}".format(response))

	else:
		cmd = b"i"
		uart.write(cmd)
		for value in options["VALUE"]:
			cmd = b"w"
			cmd += format(interface_table[options["<INTERFACE>"]]["address"], 'x').encode("utf-8") + b" "
			cmd += format(int(value,0), 'x').encode("utf-8")
			cmd += b"\n"
			uart.write(cmd)
			response = uart.readline().decode("utf-8").strip().replace('\x00','')
			if (response.lower() != "ok"):
				print("ERROR writing " + value + " to " + options["<INTERFACE>"] + " (response: " + str(response) + ")")
				exit(1)



if __name__ == "__main__":
	main()

class dbg_port:
	def __init__(self, port="/dev/ttyUSB0", baudrate=115200):
		self.uart = serial.Serial()
		self.uart.port = port
		self.uart.baudrate = baudrate
		self.uart.bytesize = serial.EIGHTBITS #number of bits per bytes
		self.uart.parity = serial.PARITY_NONE #set parity check: no parity
		self.uart.stopbits = serial.STOPBITS_ONE #number of stop bits
		self.uart.timeout = 1       # timeout in seconds
		self.uart.xonxoff = False
		self.uart.rtscts = False
		self.uart.dsrdtr = False

	def open(self):
		self.uart.open()
		cmd = b"i"
		self.uart.write(cmd) # reset interface

	def read_address(self, address):
		cmd = b"ir"+ format(address, 'x').encode("utf-8")+b"\n"
		self.uart.write(cmd)
		response = self.uart.readline().decode("utf-8").strip()
		if (response.lower() == "error"):
			raise Exception("Unable to read from adress " + str(address))
		return int(response.replace('\x00',''), 16)

	def write_address(self, address, value):
		cmd = b"w"
		cmd += format(address, 'x').encode("utf-8") + b" "
		cmd += format(value, 'x').encode("utf-8")
		cmd += b"\n"
		self.uart.write(cmd)
		response = self.uart.readline().decode("utf-8").strip().replace('\x00','')
		if (response.lower() != "ok"):
			raise Exception("ERROR writing " + value + " to address " + address + " (response: " + str(response) + ")")

	def get_id(self):
		try:
			return self.read_address(0)
		except Exception as ex:
			raise Exception(" Error while reading constant 'id' (" + str(ex) + ")" )
	def get_version(self):
		try:
			return self.read_address(1)
		except Exception as ex:
			raise Exception(" Error while reading constant 'version' (" + str(ex) + ")" )
	def get_sw_switches(self):
		try:
			return self.read_address(2)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_switches' (" + str(ex) + ")" )
	def set_sw_switches(self, value):
		try:
			self.write_address(2, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_switches' (" + str(ex) + ")" )
	def get_sw_keys(self):
		try:
			return self.read_address(3)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_keys' (" + str(ex) + ")" )
	def set_sw_keys(self, value):
		try:
			self.write_address(3, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_keys' (" + str(ex) + ")" )
	def get_actual_switches(self):
		try:
			return self.read_address(4)
		except Exception as ex:
			raise Exception(" Error while reading input 'actual_switches' (" + str(ex) + ")" )
	def get_actual_keys(self):
		try:
			return self.read_address(5)
		except Exception as ex:
			raise Exception(" Error while reading input 'actual_keys' (" + str(ex) + ")" )
	def get_ledr(self):
		try:
			return self.read_address(6)
		except Exception as ex:
			raise Exception(" Error while reading input 'ledr' (" + str(ex) + ")" )
	def get_ledg(self):
		try:
			return self.read_address(7)
		except Exception as ex:
			raise Exception(" Error while reading input 'ledg' (" + str(ex) + ")" )
	def get_hex(self):
		try:
			return self.read_address(8)
		except Exception as ex:
			raise Exception(" Error while reading input 'hex' (" + str(ex) + ")" )
	def get_swic(self):
		try:
			return self.read_address(9)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'swic' (" + str(ex) + ")" )
	def set_swic(self, value):
		try:
			self.write_address(9, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'swic' (" + str(ex) + ")" )
	def get_sw_res_n(self):
		try:
			return self.read_address(10)
		except Exception as ex:
			raise Exception(" Error while reading output signal 'sw_res_n' (" + str(ex) + ")" )
	def set_sw_res_n(self, value):
		try:
			self.write_address(10, value)
		except Exception as ex:
			raise Exception(" Error while writing to output 'sw_res_n' (" + str(ex) + ")" )
