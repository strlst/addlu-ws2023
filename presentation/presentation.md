---
title: ADD LU WS2023
author:
- Norbert Tremurici (11907086)
- Ilija Vorontsov (11913609)
date: 24th January 2024
---

# **Metastability**

## Our first suspicion

![$\downarrow$ Metastability](../report/graphics/meta-edge.png)

## fall@lvl Measurements

![Measurements using fall@lvl, regular case](../report/graphics/meta-fall-at-level-regular.png)

## fall@lvl Measurements

![Measurements using fall@lvl, metastable case](../report/graphics/meta-fall-at-level-metastable.png)

## Evaluated data

![Observed erroneous behvaior data grouped](../report/graphics/meta-data1.png)

## Metastability Measurement

![Visible skew rate changes due to metastability + measurements](../report/graphics/meta-measurements.png)

## Evaluated data

![Logarithmic buckets for $\tau_c \approx 45 ps$ estimation](../report/graphics/meta-data.png)

# **Clock Domain Crossing**

## Clock Domain Crossing

We are looking out for:

- Indicators of missing flow control
- Indicators of missing synchronisation

![\label{fig:no-ack}Abstract `cdc` system without `ack`](../report/graphics/cdc-nohandshake.jpg)

## Missing flow control?

- Sometimes data is not received in time and yet still changed by the source

![\label{fig:cdc-upset-flow}Observed upset due to missing flow control](../report/graphics/cdc-upset-flow.jpg)

## Missing synchronization?

- Sometimes data is received at the wrong time

![\label{fig:cdc-upset-sync}Observed upset due to missing synchronization](../report/graphics/cdc-upset-sync.png)

## Flow Control

- Basic addition of `ack` signal

![\label{fig:handshake}Handshaking protocol](../report/graphics/cdc-handshake.jpg)

## With flow control

- Naive flow control implementation even leads to deadlock
- There is still erroneous behavior

![\label{fig:maximal}Maximal distance between observed upset events](../report/graphics/cdc-maxtbu.png)

## With flow control

- Naive flow control implementation even leads to deadlock
- There is still erroneous behavior

![\label{fig:minimal}Minimal distance between observed upset events](../report/graphics/cdc-mintbu.png)

## MTBU

$$
MTBU=\frac{TBU_{max}+TBU_{min}}2=34.22\mu s
$$

![\label{fig:modulation}Frequency modulation of `clk_src`](../report/graphics/cdc-modulation.jpg){ width=60% }

## Time for synchronization

- Addition of registers on both sides

![\label{fig:handshake}Classic handshaking protocol](../report/graphics/cdc-flow-sync.jpg)

## $t_{res}$?

- We know that:

$$
MTBU=MTBU_0 e^{k\frac{t_{res}}{\tau_c}}
$$

- But at the time of submission, we haven't found all parameters
- Slow 1200 mV 85C Model Quartus:

$$
\begin{aligned}
f_{max} &= 318MHz \to T_{max} \approx 3 ns \\
f_{clk} &= 60 MHz \to T_{clk} \approx 16.66 ns
\end{aligned}
$$

$$
\begin{aligned}
t_{res,max} = 0 &= T_{max} - t_{clk2out} - t_{su} - t_{comb} \\
&= T_{max} - t_{clk2out} - t_{su} \to t_{su} + t_{clk2out} \approx 3 ns \\
t_{res} &= T_{clk} - t_{clk2out} - t_{su} \approx 13.66 ns \\
\end{aligned}
$$

## Calculated stages

$$
MTBU=MTBU_0 e^{k\frac{t_{res}}{\tau_c}}
$$

- After plugging in some values

$$
3.1547 \cdot 10^{13} = 34.22 \cdot 10^{-6} \cdot e^{k \cdot \frac{13.33 \cdot 10^{-9}}{4.5 \cdot 10^{-11}}}
$$

- Which yields...

$$
k \approx 0.139642
$$

- But not quite right...

## Performance

- Using 3 synchronizer stages, our latency is:

|$L_{max}$|$L_{min}$|$L_{avg}$|
|---|---|---|
|10|8|9|

- And our throughput is: one word every 9 cycles on average yields $\approx 6.66 \cdot 10^6 \text{ words}/s$

![\label{fig:handshake}Handshaking protocol](../report/graphics/cdc-flow-sync.jpg)

## Trigger Warning

![Trigger used to find erroneous events](../report/graphics/cdc-trigger.png)

**STILL** `Waiting [Logic Analyzer-1] for trigger in Trigger Step 1..…`

# Asynchronous Control Circuits

![LED Patterns to be generated](../stgs/img/led_patterns.png){ width=60% }

## Methodology

- Use Workcraft to create LED controller
- Invert inputs/outputs in STG to create `ack` controller
- Generate Verilog files
    - using MPSat *Standard C-gate* synthesis
- Apply minimal post-processing
- Wrap generated files in VHDL
- Success

## Pattern **A**

![LED Patterns to be generated](../stgs/img/led_patterns.png){ width=60% }

## LED Controller **A**

![\label{fig:stg-la}STG for LED controller of pattern a](../report/graphics/stg-la.png)

## `ack` Controller **A**

![\label{fig:stg-aa}STG for `ack` controller of pattern a](../report/graphics/stg-aa.png)

## LED Controller **A** circuit

![\label{fig:stg-ila}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller a](../report/graphics/stg-ila.png){ width=65% }

## `ack` Controller **A** circuit

![\label{fig:stg-iaa}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller a](../report/graphics/stg-iaa.png)

## Pattern **B**

![LED Patterns to be generated](../stgs/img/led_patterns.png){ width=60% }

## LED Controller **B**

![\label{fig:stg-lb}STG for LED controller of pattern b](../report/graphics/stg-lb.png)

## `ack` Controller **B**

![\label{fig:stg-ab}STG `ack` LED controller of pattern b](../report/graphics/stg-ab.png)

## LED Controller **B** circuit

![\label{fig:stg-ilb}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller b](../report/graphics/stg-ilb.png){ width=60% }

## `ack` Controller **B** circuit

![\label{fig:stg-iab}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller b](../report/graphics/stg-iab.png)

## Pattern **C**

![LED Patterns to be generated](../stgs/img/led_patterns.png){ width=60% }

## LED Controller **C**

![\label{fig:stg-lc}STG for LED controller of pattern c](../report/graphics/stg-lc.png)

## `ack` Controller **C**

![\label{fig:stg-ac}STG for `ack` controller of pattern c](../report/graphics/stg-ac.png)

## LED Controller **C** circuit

![\label{fig:stg-ilc}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller c](../report/graphics/stg-ilc.png){ width=60% }

## `ack` Controller **C** circuit

![\label{fig:stg-iac}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller c](../report/graphics/stg-iac.png)

## Pattern **D**

![LED Patterns to be generated](../stgs/img/led_patterns.png){ width=60% }

## LED Controller **D**

![\label{fig:stg-ld}STG for LED controller of pattern d](../report/graphics/stg-ld.png)

## `ack` Controller **D**

![\label{fig:stg-ad}STG for `ack` controller of pattern d](../report/graphics/stg-ad.png)

## LED Controller **D** circuit

![\label{fig:stg-ild}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller d](../report/graphics/stg-ild.png){ width=60% }

## `ack` Controller **D** circuit

![\label{fig:stg-iad}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller d](../report/graphics/stg-iad.png)

## Quartus RTL Viewer Results

![\label{fig:stg-block}Quartus RTL Viewer of entire design](../report/graphics/stg-block.png)

## Quartus RTL Viewer Results

![\label{fig:stg-area}Quartus RTL Viewer area comparison of LED/`ack` controllers](../report/graphics/stg-area-comparison.png){ width=50% }

## Quartus RTL Viewer Results

![\label{fig:stg-block-a-expanded}Quartus RTL Viewer expanded view of pattern generator a](../report/graphics/stg-block-a-expanded.png)

## Quartus RTL Viewer Results

![\label{fig:stg-block-b-expanded}Quartus RTL Viewer expanded view of pattern generator b](../report/graphics/stg-block-b-expanded.png)

## Quartus RTL Viewer Results

![\label{fig:stg-block-c-expanded}Quartus RTL Viewer expanded view of pattern generator c](../report/graphics/stg-block-c-expanded.png)

## Quartus RTL Viewer Results

![\label{fig:stg-block-d-expanded}Quartus RTL Viewer expanded view of pattern generator d](../report/graphics/stg-block-d-expanded.png)
