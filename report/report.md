# **Metastability Measurement**

In order to determine the $\tau$ constant for further calculations of Metastability a D-FF was received data at a rate of $27 MHz$ and sampled this data with a clock of $50 MHz$.

```jsx
 clk27 (27 MHz) +-------+
  --------------| D   Q |----- sma_j25 (connect to oscilloscope)
                |       |
                |  clk  |
                +---+---+
                    |
                    +--------- sma_j27 (connect to oscilloscope)
                    |
               clk50 (50 MHz)
```

We used the Lecroy HDO 4104A oscilloscope to measure metastability. Fig. 1 displays how rising edges are delayed due to metastability. The yellow line is the clock edge and the pink one the data edge, the oscilloscope was used in persistent mode, allowing us to see 1000 transitions at the same time. When looking at the rising edge of the yellow line, one can see that sometimes the clock comes in earlier, caused by an additional required time to resolve some metastability. 

The horizontal yellow lines depict the skew measurements taken over the current 1000 transitions, where data transitions are delayed by $1.8-1.9ns$ whilst the maximal delay is around $2.8 ns$.

![Visible skew rate changes due to metastability + measurements](graphics/meta-measurements.png)

In order to determine the value of $\tau_c$ 100 samples of the skew rate were measured and exported from the oscilloscope. These skew measurments were combined into a big file. Samples within a $5ps$ window were grouped into buckets. Over these frequencies the $\ln$ was taken, in order to be able to linearly approximate the $e^{-\frac{t}{\tau_c}}$. This curve somewhat corresponds to the upset rate, for certain upsets the "time to recover" is longer. The skew rates, that interesst us are the ones after the 1880 ps mark. They resemble the outliers, which we can approximate using a linear equasion. Since due to the low ammount of readings, there are spikes, it was enough for us to just graphically put a line through them, without using special algorithms. After the time-dropoff when moving one decade on the y scale, was determined, by reading the value. This value is equivilent to $\tau_c \approx 45 ps$.
The corresponding plot can be seen in Fig. 2.

![Logarithmic buckets for $\tau_c$ estimation](graphics/meta-data.png)

This concludes the metastability task.

## (Additional considerations: LVTTL 3.3V)

We first fixated on some curious behavior that was not the intended event to be observed.
But we included some considerations on this behavior for completeness.

The circuit uses LVTTL $3.3V$ logic so after a $\downarrow$ downwards transition the passing of the voltage level of $0.4 V$ indicates the final transition to the low level. The threshold for the $\uparrow$ upwards transitions is $2.4V$. 

After setting up the oscilloscope using signal persistency the following image (Fig.3). The upper signal (C1) represents the output (Q) of the D-FF and the lower (C2) the clock signal (50 MHz). Looking at the following figure (Fig.1) some points of metastability can be found, by looking at transitions that take different amounts of time to reach the levels for the respective upwards and downwards transition. Since the delay for the downwards transitions is greater the future focus will be on them.

![Coarse oscilloscope reading](graphics/meta-lvttl.png)

### $\downarrow$ Transitions

Figure 4 shows the case of the downward transition. 

The horizontal dashed line in the upper graph indicates the threshold of $0.4V$.  The vertical dashed line indicates the point where most downwards transitions reach the threshold and the dash-dotted line indicates the time where transitions with an upset reach the level. Using the pointer function of the oscilloscope the difference was determined to be around $6.16 ns$.

![$\downarrow$ Metastability](graphics/meta-edge.png)

These delays can also be measured using the **falltime@level** measurement function of the oscilloscope. Which allowed us to tune measure the time the signal takes to reach a certain voltage level on the downwards transition. Figures 5 and 6 show two measurements of a regular transition and a metastable one. These measurements were aggregated in `csv` files and analysed. 

![fall@lvl regular](graphics/meta-fall-at-level-regular.png)

![fall@lvl metastable](graphics/meta-fall-at-level-metastable.png)

![Observed erroneous behavior data frequency](graphics/meta-data0.png)

![Observed erroneous behvaior data grouped](graphics/meta-data1.png)

# Clock domain crossing

## Setup

In this experiment the setup was a data source which sends data to a sink on a DE2-115 FPGA board. The sinks clock was $60MHz$ and the sources clock was  $60MHz\pm1 MHz$ modulated with a modulation frequency of $100 Hz$  using a triangular modulation waveform using the Agilent/Keysight 33250A Arbitrary Waveform Generator as a external source. The signals output by the source and sink were observed using a Agilent/Keysight 16803A logic analyzer. 

## No Flow Control and Synchronisation

Initially no flow control and synchronisation was implemented on the source and sink. Like expected, upsets were visible on the logic analyser. These were caused, because of

1. Missing flow control (depicted in Figure \ref{fig:upset1}), where part of the dataword was wrongly coppied, due to bad clock phases.
    
![\label{fig:upset1}Observed upset due to missing flow control for respective bits](graphics/cdc-upset-flow.jpg)
    
2. Missing synchronisation (depicted in Figure \ref{fig:upset2}), where an upset occurred in the sink, after which the next few data words are skipped entirely. These occur when the signal is when the data edge falls into the sinks $T_0$ window and the metastable signal is carried into the circuit. 
    
![\label{fig:upset2}Another observed upset due to missing synchronisation](graphics/cdc-upset-sync.png)

## Flow Control

In order to mitigate the first type of errors it needs to be ensured that new data is only sent, when the sink has consumed the previous one. We implemented a 2-phase handshake protocol, which only depends on the current values of `req` and `ack` , if we take a look at the lectures timing diagram (Figure \ref{fig:handshake}) of such a handshake protocol, we can see, that the source can always send out new data when both `req` and `ack` are low and sets `req` . If both are high, the source can pull `req` to low and the rest of the time it waits.

![\label{fig:handshake}Classic handshaking protocol](graphics/cdc-handshake.jpg)

The errors caused by flow control went away, but we observed, that now synchronisation errors occurred in the source visible in Figure \ref{fig:maximal} (or Figure \ref{fig:minimal}), since it now has an asynchronous input too. We observed that the upsets occurrences correlated, since the `ack` signal from a metastable sink would most definitely cause metastability in the source.

In order to determine the MTBU, we tried to find the minimum and maximum time between upsets, since the clocks phase change with the triangular modulation at a rate of $100Hz$. The maximum was roughly determined, maximally zoomed out, and finely by doing cursory measurements, with increasing values, until the next measurement was less then the previous one. The Maximum TBU was measured to be $MaxTBU =67.34 \mu s$ and can be seen in Figure \ref{fig:maximal}. The Minimal-TBU was determined similarly, and is $MinTBU=1.10\mu s$ and can be seen in Figure \ref{fig:minimal}.

![\label{fig:maximal}Maximal distance between observed upset events](graphics/cdc-maxtbu.png)

![\label{fig:minimal}Minimal distance between observed upset events](graphics/cdc-mintbu.png)

Since the phase change in a linear manner, the MTBU can be calculated by taking the mean of the two values

$$
MTBU=\frac{TBU_{max}+TBU_{min}}2=34.22\mu s
$$

Since originally the data was transmitted at a rate of $60MHz$, but because of the added flow control, the sender, sends data only every 3 or 5 cycles, since now after the . By similar argument as above, the average data rate is $\frac{60MHz}{4cycles}=15MHz$ equalling a transmission every $TR=66.66 ns$. Thus since we can expect an upset to occur every $MTBU=34.22\mu s$. The probability for a transmission to cause an error equals $P=\frac{TR}{MTBU}=\frac{66.66e-9}{34.22e-6}=0.195\%$.

## Synchronisation

Regarding the $MTBU$ formula, the first part $\frac{1}{\lambda_{dat}f_{clk}T_0}$ corresponds to the rate at which the upsets are produced. This rate corresponds to the MTBU, in the case that no synchronizer stages are used, since $e^{0}=1$.
In order to now determine the number of stages for the synchronizers the formula can be algebraically transformed. There are roughly $3.1547*10^7$ seconds in a year. We approximate the resolution time 
$$
MTBU=MTBU_0 e^{k\frac{t_{res}}{\tau_c}}
$$

Here we still see, that the value of $t_{res}$ is still unknown. we know that $t_{res}=T_{clk}-t_{clk2out}-t_{su}-t_{comb}$. And we know from experience, that we get timing issues at around $f_{max} = 318MHz \rightarrow T_{max}\approx 3ns$  (Slow 1200 mV 85C Model Quartus) without any combinational logic. So we can say that $t_{res}=0=T_{max}-t_{clk2out}-t_{su}$ thus $t_{su} + t_{clk2out} \approx 3 ns$.
At a freqency of $60 MHz$ the corresponding resolution time is $t_{res}=16.66 ns - 3 ns = 13.66 ns$. 

For the amount of synchronizer stages, we tried to calculate what the required amount of synchronizer stages is using the above formula:

$$
3.1547 \cdot 10^{13} = 34.22 \cdot 10^{-6} \cdot e^{k \cdot \frac{13.33 \cdot 10^{-9}}{4.5 \cdot 10^{-11}}}
$$

Then we tried to solve for the number of stages, which yielded the result $k \approx 0.139642$.
This result is not correct in our opinion, but we are not sure where we went wrong in our calculation.
With one stage we still observed upsets, at around the rate of an upset a minute.

By using synchronizer we are reducing the latency because a transmission, which accompanies a handshake, has to travel through synchronizer stages on both sides.
If we are using a 3 stage synchronizer, our smallest possible latency would be 8 cycles.
In the worst case, which occurs if the clocks are in an unfortunate phase, we would have a latency of 10 cycles (missing the window on both sides).
Because the rate varies linearly, we can expect an average latency of 9 cycles.

Our average frequency is 60 MHz, which yields a cycle time of $16.66 ns$.
Because we can transmit one packet every 7 cycles on average, one transmission takes $150 ns$.
If we normalize this value to 1 second, we have $\approx 6.6 \cdot 10^6 \text{words}/s$ as our average throughput.

![Trigger used to find erroneous events](graphics/cdc-trigger.png)

**STILL** `Waiting [Logic Analyzer-1] for trigger in Trigger Step 1..…`

# **Asynchronous Control Circuits**

The task was solved using Workcraft, Quartus and the (remote) lab environment.
All patterns were implemented first as an STG inside Workcraft and simulated for correctness, with the ack signal as an input and the LED signals as outputs.
Then, a respective acknowledge controller was generated from the LED controller by simply setting the LED signals to be inputs and the `ack` signal an output.

The next step was to generate respective circuits using the synthesis function of Workcraft.
Automatic conflict resolution performed by Workcraft, but we will discuss conflicts more in each respective section.
For this step, several synthesis methods were tried, but we ended up using the MPSat *Standard C-element* synthesis option for all circuits.
One reason for this is that Petrify not only produced somewhat difficult to read circuits, but also that Petrify could not handle the first pattern.

To be able to iterate faster, we settled on an approach where we generate a Verilog output file for each synthesized circuit and apply minimal post-processing to bring the Verilog file in the right form.
Then the Verilog module was plugged into our VHDL code using a wrapper that feeds the necessary inputs and samples the `*_SET` and `*_nRESET` signals as they were automatically produced by the MPSat *Standard C-element* synthesis option.

Wherever Workcraft produced an output that is either directly a C-gate or resembles a C-gate in its logic, we inserted C-gates in our design.
The correct reset state was also produced in the Verilog file, but it was also directly readable from our STG specifications.

We are going to elaborate on the implementation in the penultimate section of this report, by showing the organization of our implementation of this task in the Quartus RTL viewer.

But first, we will examine all patterns in more detail.

## Pattern **a**

Like previously mentioned, we have two STGs which are basically mirrors of each other (Figure \ref{fig:stg-la} and \ref{fig:stg-aa}).
The first one is used to generate the LED controller for pattern a, while the second one is used to generate the `ack` signal for the LED controller.

![\label{fig:stg-la}STG for LED controller of pattern a](graphics/stg-la.png)

![\label{fig:stg-aa}STG for `ack` controller of pattern a](graphics/stg-aa.png)

The next figures (Figure \ref{fig:stg-ila} and \ref{fig:stg-iaa}) show the generated circuits for the LED and `ack` controllers respectively.

![\label{fig:stg-ila}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller a](graphics/stg-ila.png)

![\label{fig:stg-iaa}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller a](graphics/stg-iaa.png)

Now we come to considerations about conflict states.
First of all, we are going to assert that any combination of LEDs that appears more than once introduce a conflict, but only if their follow-up state(s) is (are) not the same.
This is relatively obvious, because we wouldn't be able to tell only by knowing a snapshots of the LEDs which transition comes next.

We can use this to find conflicts.
As can be seen, lines 0 and 4, 1 and 7, 2 and 6, as well as 3 and 5 of pattern a have an identical state but different follow-up states.
This is problematic for an implementation, since we need to be able to differentiate these events.
Thus several conflict states have to be introduced.
Workcraft generated a total of 6 conflict state signals to deal with this problem.

## Pattern **b**

For this pattern, we again have two STGs (Figure \ref{fig:stg-lb} and \ref{fig:stg-ab}).

![\label{fig:stg-lb}STG for LED controller of pattern b](graphics/stg-lb.png)

![\label{fig:stg-ab}STG `ack` LED controller of pattern b](graphics/stg-ab.png)

The next figures (Figure \ref{fig:stg-ilb} and \ref{fig:stg-iab}) show the generated circuits for the LED and `ack` controllers respectively.

![\label{fig:stg-ilb}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller b](graphics/stg-ilb.png)

![\label{fig:stg-iab}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller b](graphics/stg-iab.png)

For this pattern, we have some unique states in lines 0, 2, 4 and 6.
But their follow-up states are problematic, as they are all identical (empty lines).
This is solved with the help of two conflict state signals generated by Workcraft.

Note that this pattern has very simple transitions, for example the `ack` signal can be generated in quite a simple fashion.
In one phase, all LEDs have to be unset, in the next, one of the LEDs has to be set.

## Pattern **c**

For this pattern, we again have two STGs (Figure \ref{fig:stg-lc} and \ref{fig:stg-ac}).

![\label{fig:stg-lc}STG for LED controller of pattern c](graphics/stg-lc.png)

![\label{fig:stg-ac}STG for `ack` controller of pattern c](graphics/stg-ac.png)

The next figures (Figure \ref{fig:stg-ilc} and \ref{fig:stg-iac}) show the generated circuits for the LED and `ack` controllers respectively.

![\label{fig:stg-ilc}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller c](graphics/stg-ilc.png)

![\label{fig:stg-iac}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller c](graphics/stg-iac.png)

The conflicts for this pattern are as in pattern b, we have four empty lines with conflicting states (lines 0, 2, 4 and 6), whereas the other lines have unique states (1, 3, 5 and 7).
To solve this, we are using three conflict state signals generated by Workcraft.

## Pattern **d**

For the final pattern, we again have two STGs (Figure \ref{fig:stg-ld} and \ref{fig:stg-ad}).

![\label{fig:stg-ld}STG for LED controller of pattern d](graphics/stg-ld.png)

![\label{fig:stg-ad}STG for `ack` controller of pattern d](graphics/stg-ad.png)

The next figures (Figure \ref{fig:stg-ild} and \ref{fig:stg-iad}) show the generated circuits for the LED and `ack` controllers respectively.

![\label{fig:stg-ild}Circuit generated using the MPSat *Standard C-gate* synthesis option for LED controller d](graphics/stg-ild.png)

![\label{fig:stg-iad}Circuit generated using the MPSat *Standard C-gate* synthesis option for `ack` controller d](graphics/stg-iad.png)

The final pattern has a nice structure.
The last LED, `LED3`, acts as a toggling signal.
The entire pattern looks similar to a kind of gray code, although it is not a gray code.
The remarkable thing here is that there are no conflict states.
This can be easily seen, as each line has a unique assignment of LEDs.
This is already enough, but their follow-up states are also unique.
So for this pattern, we are not using any conflict state signals.

## Quartus RTL Viewer Results

Figure \ref{fig:stg-block} shows the entire design.
Figure \ref{fig:stg-area} shows an area comparison of all 4 pattern generator units.

![\label{fig:stg-block}Quartus RTL Viewer of entire design](graphics/stg-block.png)

![\label{fig:stg-area}Quartus RTL Viewer area comparison of LED/`ack` controllers](graphics/stg-area-comparison.png)

We also include expanded views of all pattern generators for comparison.
The expanded pattern generators can be seen in Figure \ref{fig:stg-block-a-expanded}, \ref{fig:stg-block-b-expanded}, \ref{fig:stg-block-c-expanded} and \ref{fig:stg-block-d-expanded}.

![\label{fig:stg-block-a-expanded}Quartus RTL Viewer expanded view of pattern generator a](graphics/stg-block-a-expanded.png)

![\label{fig:stg-block-b-expanded}Quartus RTL Viewer expanded view of pattern generator b](graphics/stg-block-b-expanded.png)

![\label{fig:stg-block-c-expanded}Quartus RTL Viewer expanded view of pattern generator c](graphics/stg-block-c-expanded.png)

![\label{fig:stg-block-d-expanded}Quartus RTL Viewer expanded view of pattern generator d](graphics/stg-block-d-expanded.png)
