library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		clk50   : in  std_logic; -- Y2
		clk27   : in  std_logic; -- B14
		
		-- onboard SMA connectors
		sma_clkin : in std_logic;  -- AH14
		sma_clkout : out std_logic; -- AE23
		
		-- daughter board SMA connectors
		sma_j25 : out std_logic;  -- V23
		sma_j27 : out std_logic;  -- V24
		sma_j26 : in std_logic;   -- Y27
		sma_j28 : in std_logic    -- Y28
	);
end entity;


architecture arch of top is
begin
	sma_j27 <= clk50;

	sync : process(clk50)
	begin
		if rising_edge(clk50) then
			sma_j25 <= clk27;
		end if;
	end process;

end architecture;
