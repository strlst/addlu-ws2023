# Metastability Measurement

For this exercise implement a single D flip flop in the architecture of the top entity.
Connect its clock input to the 50 MHz clock source of the FPGA board (`clk50`) and its data input to the (independent and uncorrelated) 27 MHz clock source (`clk27`). 

Your task is to measure the metastability behavior of this flip flop using the Lecroy HDO 4104A oscilloscope.
For that purpose you have to connect the flip flop’s clock input signal as well as its data output to the scope using the outputs `sma_j25` and `sma_j27`.
These outputs represent the SMA connectors labeled J25 and J27 on the daughter board attached to the DE2-115 FPGA board provided in the lab.


```
 clk27 (27 MHz) +-------+
  --------------| D   Q |----- sma_j25 (connect to oscilloscope)
                |       |
                |  clk  |
                +---+---+
                    |
                    +--------- sma_j27 (connect to oscilloscope)
                    |
               clk50 (50 MHz)
```

Boot the oscilloscope and connect the SMA cables using the provided adapters and setup a trigger that triggers on either the rising or the falling edge of the data output (Q) of the flip flop.
If you now activate infinite signal persistency (Display -> Persistence Setup) you should already be able to see that in some cases the time between the clock and data edge deviates from its "usual" value, i.e., the clock-to-output delay is increased.
This is due to the flip flop becoming metastable.
Let this setup run for a few minutes and add a screenshot of the resulting waveform to you lab report.
What is the maximum deviation that you measured?


Now you are going to use the oscilloscope to estimate the $`\tau`$ parameter of the flip flop.
Go to the measurement setup menu (Measure -> Measure Setup) and configure the P1 trace to measure the delay between the clock and the data edge.
For that purpose you can use the "DTime@level" or the "Skew" measurement option.
Additionally, configure the trace F1 to record the trend of P1.
F1 will now always record and store the last 1000 delay measurements.
The F1 trace can then be saved to a CSV for further processing (File -> Save).
Record at least 35 sets of 1000 delay measurements.
Be sure to wait a sufficient amount of time between presses of the "Save Now" button, s.t. the oscilloscope has enough time to record a new set of values, otherwise this experiment will not work!
If you click "Clear Sweep" in the F1 trace menu, you can see how long it takes for the scope to record 1000 values.

Use the recorded data to estimate the $`\tau`$ parameter of the flip flop.
Make sure to document your approach and add the necessary plots to your report.
Create the characteristic metastability plot as discussed in the lecture.
Consult the slides of Lecture 4 to have a rough guidance on how the this plots should look like.
Note, however, that in your experiment who won't see any slave metastability!
